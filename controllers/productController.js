const Product = require('../models/products');

//get all task
module.exports.getAllProducts = () =>{
	return Product.find({}).then(result =>{
		return result;
	})
}

//add product
module.exports.createProduct = (requestBody)=>{
			let newProduct = new Product({
			name:requestBody.name,
			price:requestBody.price
			})
			return newProduct.save().then((product,err)=>{
			if(err){
				console.error(err)
			}else{
			return product;
			}
			})
		}

//get single product
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then((result,error)=>{
		if(error){
			console.error(error)
			return false;
		}else{
			return result;
		}
	})
}

//delete a single product
module.exports.deleteProduct = (productId) =>{
	return Product.findByIdAndRemove(productId).then((result,error)=>{
		if(error){
			console.error(error)
			return false;
		}else{
			return result;
		}
	})
}