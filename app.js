const express = require('express');
const mongoose = require('mongoose');
const productRoutes = require('./routes/productRoutes');

const app = express();
const port = 3001;

app.use(express.urlencoded({extended:true}));
app.use(express.json());

app.use('/products',productRoutes);


mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.9s26b.mongodb.net/s31-REST-activity?retryWrites=true&w=majority',{
	useNewUrlParser:true,
	useUnifiedTopology:true
}
);



app.listen(port,()=>console.log(`Server running at port: ${port}`));