const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController')

router.get('/',(req,res)=>{
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

router.post('/',(req,res)=>{
	productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
})

router.get('/:id',(req,res)=>{
	productController.getProduct(req.params.id).then(resultFromController => res.send(resultFromController))
})

router.delete('/delete/:id',(req,res)=>{
	productController.deleteProduct(req.params.id).then(resultFromController => res.send(resultFromController))
})

module.exports = router;